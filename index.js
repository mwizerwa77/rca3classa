let express = require("express");
let app = express();

app.get("/", (req, res) => res.send("Welcome to jenkins demo"));
app.get("/api/students", (req, res) => {
  res.send({ id: 1, name: "stecie", class: "year three class a student" });
});
app.listen(5000, () => console.log("listening on port 5000"));
